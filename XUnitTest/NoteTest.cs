﻿using ChordProFactory.MusicalNotation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Collections;
using ChordProFactory;

namespace XUnitTest
{

    public class NoteTest
    {
        [Theory]
        [ClassData(typeof(NoteTestTransposeData))]
        public void TransposeTest(Note note, int steps, TransposeOption option, Note expectedNote)
        {
            note.Transpose(steps, option);
            Assert.True(expectedNote.Equals(note));
        }

        [Fact]
        public void EqualsTest()
        {
            Note note = new Note(Letter.A);
            Note note2 = new Note(Letter.A, Accidental.None);
            Note note3 = new Note(Letter.A, Accidental.Bemol);
            Assert.True(note.Equals(note2));
            Assert.False(note.Equals(note3));
        }
    }

    public class NoteTestTransposeData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { new Note(Letter.A, Accidental.Bemol), 5, TransposeOption.SharpMode, new Note(Letter.C, Accidental.Sharp) };
            yield return new object[] { new Note(Letter.A, Accidental.Bemol), 5, TransposeOption.BemolMode, new Note(Letter.D, Accidental.Bemol) };
            yield return new object[] { new Note(Letter.B, Accidental.None), 14, TransposeOption.SharpMode, new Note(Letter.C, Accidental.Sharp) };
            yield return new object[] { new Note(Letter.C, Accidental.Bemol), 2, TransposeOption.SharpMode, new Note(Letter.C, Accidental.Sharp) };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}