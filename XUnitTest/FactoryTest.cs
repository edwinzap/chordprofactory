using ChordProFactory;
using ChordProFactory.MusicalNotation;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace XUnitTest
{
    public class FactoryTest
    {
        private static Factory factory;

        public FactoryTest()
        {
            factory = new Factory();
        }

        //[Theory]
        //[InlineData(@"{artist: Imagine Dragons }", "Imagine Dragons")]
        //[InlineData(@"{a: Imagine Dragons }", "Imagine Dragons")]
        //public void TestGetArtist(string test, string result)
        //{
        //    bool r = factory.TryParseArtist(test, out string artist);

        //    Assert.True(r);
        //    Assert.NotNull(artist);
        //    Assert.Equal(artist, result);
        //}

        //[Theory]
        //[InlineData("{title: Radioactive }", "Radioactive")]
        //[InlineData("{t: Radioactive   }", "Radioactive")]
        //[InlineData("{t: Radioactive } azeazeaze {artist:azeazeae}", "Radioactive")]
        //public void TestGetTitle(string test, string result)
        //{
        //    bool r = factory.TryParseTitle(test, out string title);

        //    Assert.True(r);
        //    Assert.NotNull(title);
        //    Assert.Equal(title, result);
        //}

        //[Theory]
        //[InlineData(@"{subtitle: Imagine Dragons }", "Imagine Dragons")]
        //[InlineData(@"{St: Imagine Dragons }", "Imagine Dragons")]
        //[InlineData(@"{st: Imagine Dragons } azeazeaez[] {qsdqs}", "Imagine Dragons")]
        //public void TestGetSubtitle(string test, string result)
        //{
        //    bool r = factory.TryParseSubtitle(test, out string subtitle);

        //    Assert.True(r);
        //    Assert.NotNull(subtitle);
        //    Assert.Equal(subtitle, result);
        //}

        //[Fact]
        //public void TestGetKey()
        //{
        //    string test1 = "{key: C}";
        //    string test2 = "{key:Am}";
        //    string test3 = "{ Key:C#} azeazeaze {artist:azeazeae}";

        //    Key result1 = new Key(new Note(Letter.C, Accidental.None), Mode.Major);
        //    Key result2 = new Key(new Note(Letter.A, Accidental.None), Mode.Minor);
        //    Key result3 = new Key(new Note(Letter.C, Accidental.Sharp), Mode.Major);

        //    Dictionary<string, Key> tests = new Dictionary<string, Key>
        //    {
        //        { test1, result1 },
        //        { test2, result2 },
        //        { test3, result3 }
        //    };
        //    foreach (var item in tests)
        //    {
        //        bool r = factory.TryParseKey(item.Key, out Key key);

        //        Assert.True(r);
        //        Assert.NotNull(key);
        //        Assert.Equal(key.Note.ToString(), item.Value.Note.ToString());
        //    }
        //}


        //[Theory]
        //[InlineData("{capo: 2}", 2)]
        //[InlineData("{Capo:  10} azeazeaze {artist:azeazeae}", 10)]
        //public void TestGetCapo(string test, int result)
        //{
        //    factory.TryParseCapo(test, out int capo);
        //    Assert.Equal(capo, result);
        //}


        //[Theory]
        //[InlineData("{duration: 2:00}", "00:02:00")]
        //[InlineData("{duration:02:55}", "00:02:55")]
        //[InlineData("{duration:4:58}", "00:04:58")]
        //public void TestGetDuration(string test, string result)
        //{
        //    bool r = factory.TryParseDuration(test, out TimeSpan duration);

        //    Assert.True(r);
        //    TimeSpan.TryParse(result, out TimeSpan resultDuration);
        //    Assert.Equal(duration, resultDuration);
        //}

        [Theory]
        [InlineData(@"D:\Documents\#Temp\Flashlight.txt")]
        public void ParseTextFormat(string path)
        {
            string text = File.ReadAllText(path);
            var chordpro = factory.ParseTextFormatSong(text);

            string printVersion = chordpro.PrintToChordProFormat();
            Assert.NotNull(chordpro);
        }

        [Theory]
        //[InlineData(@"D:\Dropbox\Chopro\Lettre � France.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Lettre � France.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Issues.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\ChordPro Edition\Main Girl.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Rain.chopro")]
        //[InlineData(@"D:\Dropbox\Chopro\Issues.chopro")]
        [InlineData(@"D:\Dropbox\Chopro\Flashlight.chopro")]
        public void PrintToTextFormat(string path)
        {
            string text = File.ReadAllText(path);
            var chordpro = factory.ParseChordProFormatSong(text);
            
            string printVersion = chordpro.PrintToTextFormat();
            Assert.NotNull(chordpro);
        }

        [Theory]
        //[InlineData(@"D:\Dropbox\Chopro\Lettre � France.chopro")]
        [InlineData(@"D:\Dropbox\Chopro\Flashlight.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Lettre � France.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Issues.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\ChordPro Edition\Main Girl.chopro")]
        //[InlineData(@"C:\Users\forge\Dropbox\Chopro\Rain.chopro")]
        //[InlineData(@"D:\Dropbox\Chopro\Issues.chopro")]
        public void PrintToChordProFormat(string path)
        {
            string text = File.ReadAllText(path);
            var chordpro = factory.ParseChordProFormatSong(text);

            string printVersion = chordpro.PrintToChordProFormat();
            Assert.NotNull(chordpro);
        }

        [Theory]
        [InlineData(@"D:\Dropbox\Chopro\Lettre � France.chopro")]
        [InlineData(@"D:\Dropbox\Chopro\9 crimes.chopro")]
        public void TransposeSongWithNote(string path)
        {
            string text = File.ReadAllText(path);
            var chordpro = factory.ParseChordProFormatSong(text);

            string printVersion = chordpro.PrintToTextFormat();
            chordpro.Transpose(new Note(Letter.C));

            string transposedVersion = chordpro.PrintToTextFormat();
            Assert.NotNull(chordpro);
        }

        [Theory]
        //[InlineData(@"D:\Dropbox\Chopro\Lettre � France.chopro")]
        [InlineData(@"D:\Dropbox\Chopro\9 crimes.chopro")]
        public void TransposeSongWithSteps(string path)
        {
            string text = File.ReadAllText(path);
            var chordpro = factory.ParseChordProFormatSong(text);

            string printVersion = chordpro.PrintToTextFormat();
            chordpro.Transpose(7);

            string transposedVersion = chordpro.PrintToTextFormat();
            Assert.NotNull(chordpro);
        }

    }
}
