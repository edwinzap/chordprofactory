﻿using System;
using System.Text.RegularExpressions;
using ChordProFactory.ChordProComponent;
using ChordProFactory.ChordProComponents;
using ChordProFactory.MusicalNotation;

namespace ChordProFactory
{
    public class Factory
    {

        public ChordPro ParseChordProFormatSong(string file)
        {
            var parser = new ChordProFormatParser();
            return parser.ParseSong(file);
        }

        public ChordPro ParseTextFormatSong(string file)
        {
            var parser = new TextFormatParser();
            return parser.ParseSong(file);
        }
    }
}
