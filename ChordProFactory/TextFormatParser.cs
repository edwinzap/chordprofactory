﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using ChordProFactory.ChordProComponent;
using ChordProFactory.ChordProComponents;

namespace ChordProFactory
{
    public class TextFormatParser : BaseParser
    {
        private const string ONLY_CHORD_PATTERN = @"(^| )" + CHORD_PATTERN + @"($| )";
        private const string CHORUS_COMMENT_PATTERN = @"^[^\w]*chorus[^\w]*$";
        private const string VERSE_COMMENT_PATTERN = @"^[^\w]*verse.*$";
        private const string COMMENT_PATTERN = @"^[^\w]*(intro|outro|interlude|instr).*$";
        private const string ONLY_SPECIAL_CHARS = @"^[^\w]*$";

        public override ChordPro ParseSong(string file)
        {
            String[] lines = file.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var chordPro = new ChordPro();

            for (int i = 0; i < lines.Length - 1; i++)
            {
                var line = lines[i];

                //Blank line
                if (IsBlankLine(line))
                {
                    chordPro.Blocks.Add(new BlankBlock());
                    continue;
                }

                if (IsComment(line))
                {
                    chordPro.Blocks.Add(new CommentBlock(line));
                    continue;
                }

                //Tab line
                if (IsTabLine(line))
                {
                    var tabLines = new List<String>();
                    while (IsTabLine(line))
                    {
                        tabLines.Add(line);
                        line = lines[++i];
                    }
                    chordPro.Blocks.Add(ParseTabBlock(tabLines));
                    i--;
                    continue;
                }

                //Verse block
                var verseLines = new List<string>();
                //Check if it's a chorus
                var isChorus = IsChorusComment(line);
                if (isChorus || IsVerseComment(line))
                {
                    chordPro.Blocks.Add(new CommentBlock(line));
                    line = lines[++i];
                }
                while (!IsTabLine(line) && !IsBlankLine(line) && i < lines.Length)
                {
                    if (!IsSpecialCharsOnly(line))
                        verseLines.Add(line);
                    
                    line = lines[++i];
                }

                if(verseLines.Count > 0)
                {
                    if (isChorus)
                        chordPro.Blocks.Add(ParseChorusBlock(verseLines));
                    else
                        chordPro.Blocks.Add(ParseVerseBlock(verseLines));
                }

                //Remove last incrementation
                i--;
            }

            return chordPro;
        }

        private Block ParseTabBlock(List<string> lines)
        {
            var tab = new TabBlock();
            for (int i = 0; i < lines.Count; i++)
            {
                var line = lines[i];
                var tabLine = new TabLine
                {
                    Text = Regex.Replace(line, FINGER_PATTERN, "")
                };
                var matches = Regex.Matches(line, FINGER_PATTERN);
                foreach (Match match in matches)
                {
                    tabLine.Fingers.Add(match.Index, int.Parse(match.Value));
                }
                tab.TabLines.Add(tabLine);
            }
            return tab;
        }

        private List<Line> ParseChordProLines(List<string> lines)
        {
            var chordProLines = new List<Line>();
            for (int i = 0; i < lines.Count; i++)
            {
                var line = lines[i];
                var chordProLine = new Line();

                if (IsChordLine(line))
                {
                    //There is a chord line
                    if (i++ < lines.Count - 1)
                    {
                        var secondLine = lines[i];
                        if (!IsChordLine(secondLine))
                        {
                            chordProLine.Lyric = secondLine;
                        }

                        var chordMatches = Regex.Matches(line, ONLY_CHORD_PATTERN);
                        foreach (Match chordMatch in chordMatches)
                        {
                            var chordString = chordMatch.Groups["chord"].Value;
                            int chordIndex = chordMatch.Groups["chord"].Index;

                            //Chords
                            var chord = new PlacedChord(new Chord(chordString), chordIndex);
                            chordProLine.Chords.Add(chord);
                        }
                    }
                }
                else
                {
                    chordProLine.Lyric = line;
                }
                chordProLines.Add(chordProLine);
            }
            return chordProLines;
        }

        private VerseBlock ParseVerseBlock(List<string> lines)
        {
            var verse = new VerseBlock();
            verse.Lines.AddRange(ParseChordProLines(lines));
            return verse;
        }

        private ChorusBlock ParseChorusBlock(List<string> lines)
        {
            var chorus = new ChorusBlock();
            chorus.Lines.AddRange(ParseChordProLines(lines));
            return chorus;
        }

        private bool IsChordLine(string line)
        {
            string lineWithoutChords = Regex.Replace(line, ONLY_CHORD_PATTERN, "");
            return Regex.IsMatch(line, ONLY_CHORD_PATTERN) && string.IsNullOrEmpty(lineWithoutChords.Trim());
        }

        private bool IsVerseComment(string line)
        {
            return Regex.IsMatch(line, VERSE_COMMENT_PATTERN, RegexOptions.IgnoreCase);
        }

        private bool IsChorusComment(string line)
        {
            return Regex.IsMatch(line, CHORUS_COMMENT_PATTERN, RegexOptions.IgnoreCase);
        }

        private bool IsSpecialCharsOnly(string line)
        {
            return Regex.IsMatch(line, ONLY_SPECIAL_CHARS);
        }

        private bool IsComment(string line)
        {
            return Regex.IsMatch(line, COMMENT_PATTERN, RegexOptions.IgnoreCase);
        }
    }
}
