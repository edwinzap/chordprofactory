﻿namespace ChordProFactory
{
    public abstract class CustomEnum<T>
    {
        protected CustomEnum(T value)
        {
            Value = value;
        }
        public T Value { get; private set; }

    }
}
