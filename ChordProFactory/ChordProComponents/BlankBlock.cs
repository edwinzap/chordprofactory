﻿using System;
using System.Collections.Generic;
using System.Text;
using ChordProFactory.MusicalNotation;

namespace ChordProFactory.ChordProComponent
{
    public class BlankBlock : Block
    {
        public override string PrintToChordProFormat()
        {
            return string.Empty;
        }

        public override string PrintToTextFormat()
        {
            return string.Empty;
        }
    }
}
