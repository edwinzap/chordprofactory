﻿using ChordProFactory.MusicalNotation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChordProFactory.ChordProComponents
{
    public class TabLine:ChordProElement
    {

        public TabLine()
        {
            Fingers = new Dictionary<int, int>();
        }
        public string Text { get; set; }
        public Dictionary<int, int> Fingers { get; set; }

        public override string PrintToChordProFormat()
        {
            return PrintToTextFormat();
        }

        public override string PrintToTextFormat()
        {
            string text = Text;
            foreach (var finger in Fingers)
            {
                text = text.Insert(finger.Key, finger.Value.ToString());
            }
            return text;
        }
    }
}
