﻿using ChordProFactory.ChordProComponents;
using ChordProFactory.MusicalNotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChordProFactory.ChordProComponent
{
    public class ChordPro : ChordProElement
    {
        public ChordPro()
        {
            Blocks = new List<Block>();
        }

        public string Artist { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Album { get; set; }
        public Key Key { get; set; }
        public int Capo { get; set; }
        public TimeSpan Duration { get; set; }

        public List<Block> Blocks { get; set; }

        public override string PrintToTextFormat()
        {
            //Concat chordPro infos
            List<string> infos = new List<string>();
            
            if (!string.IsNullOrEmpty(Title))
                infos.Add(Title);
            if (!string.IsNullOrEmpty(Artist))
                infos.Add(Artist);
            if (!string.IsNullOrEmpty(Subtitle))
                infos.Add(Subtitle);
            if (!string.IsNullOrEmpty(Album))
                infos.Add(Album);
            if (Duration != null && Duration.TotalSeconds > 0)
                infos.Add("Duration: " + Duration.ToString(@"mm\:ss"));
            if (Key!=null)
                infos.Add("Key: " + Key.ToString());


            //Concat chordPro content
            var content = new List<String>();
            foreach (var block in Blocks)
            {
                content.Add(block.PrintToTextFormat());
            }

            string concatInfos = string.Join(Environment.NewLine, infos.ToArray());
            string concatContent = string.Join(Environment.NewLine, content.ToArray());

            //Concat infos and content if infos is not empty
            if (!string.IsNullOrEmpty(concatInfos))
            {
                concatContent = string.Join(Environment.NewLine, concatInfos, concatContent);
            }
            return concatContent;
        }

        public override string PrintToChordProFormat()
        {
            //Concat chordPro infos
            var infos = new List<string>();
            
            if (!string.IsNullOrEmpty(Artist))
                infos.Add(string.Concat("{artist:", Artist, "}"));
            if (!string.IsNullOrEmpty(Title))
                infos.Add(string.Concat("{title:", Title, "}"));
            if (!string.IsNullOrEmpty(Subtitle))
                infos.Add(string.Concat("{subtitle:", Subtitle, "}"));
            if (!string.IsNullOrEmpty(Album))
                infos.Add(string.Concat("{album:", Album, "}"));
            if (Duration!= null && Duration.TotalSeconds > 0)
                infos.Add(string.Concat("{duration:", Duration.ToString(@"mm\:ss"), "}"));
            if (Key != null)
                infos.Add(string.Concat("{key:", Key.ToString(), "}"));


            //Concat chordPro content
            var content = new List<String>();
            foreach (var block in Blocks)
            {
                content.Add(block.PrintToChordProFormat());
            }

            string concatInfos = string.Join(Environment.NewLine, infos.ToArray());
            string concatContent = string.Join(Environment.NewLine, content.ToArray());

            //Concat infos and content if infos is not empty
            if (!string.IsNullOrEmpty(concatInfos))
            {
                concatContent = string.Join(Environment.NewLine, concatInfos, concatContent);
            }
            return concatContent;
        }

        public override void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            Key.Note.Transpose(steps, option);
            foreach (var block in Blocks)
            {
                block.Transpose(steps, option);
            }
        }

        public void Transpose(int steps)
        {
            Transpose(steps, GetBestTransposeOption());
        }

        public void Transpose(Note targetKey)
        {
            var option = GetBestTransposeOption();
            int steps = Note.GetStepsBetweenNotes(Key.Note, targetKey);
            Transpose(steps, option);
        }

        private TransposeOption GetBestTransposeOption()
        {
            var option = TransposeOption.SharpMode;
            if (Key.Mode == Mode.Major)
            {
                option = Modes.Where(m => m.Item1.Note.ToString() == Key.Note.ToString()).FirstOrDefault().Item3;
            }
            else
            {
                option = Modes.Where(m => m.Item2.Note.ToString() == Key.Note.ToString()).FirstOrDefault().Item3;
            }

            return option;
        }

        private static readonly List<Tuple<Key, Key, TransposeOption>> Modes = new List<Tuple<Key, Key, TransposeOption>>
        {
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.C), Mode.Major), new Key(new Note(Letter.A), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.G), Mode.Major), new Key(new Note(Letter.E), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.D), Mode.Major), new Key(new Note(Letter.B), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.A), Mode.Major), new Key(new Note(Letter.F, Accidental.Sharp), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.E), Mode.Major), new Key(new Note(Letter.C, Accidental.Sharp), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.B), Mode.Major), new Key(new Note(Letter.G, Accidental.Sharp), Mode.Minor), TransposeOption.SharpMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.F, Accidental.Sharp), Mode.Major), new Key(new Note(Letter.D, Accidental.Sharp), Mode.Minor), TransposeOption.SharpMode),

             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.F), Mode.Major), new Key(new Note(Letter.D), Mode.Minor), TransposeOption.BemolMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.B, Accidental.Bemol), Mode.Major), new Key(new Note(Letter.G), Mode.Minor), TransposeOption.BemolMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.E, Accidental.Bemol), Mode.Major), new Key(new Note(Letter.C), Mode.Minor), TransposeOption.BemolMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.A, Accidental.Bemol), Mode.Major), new Key(new Note(Letter.F), Mode.Minor), TransposeOption.BemolMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.D, Accidental.Bemol), Mode.Major), new Key(new Note(Letter.B, Accidental.Bemol), Mode.Minor), TransposeOption.BemolMode),
             new Tuple<Key, Key, TransposeOption>(new Key(new Note(Letter.G, Accidental.Bemol), Mode.Major), new Key(new Note(Letter.E, Accidental.Bemol), Mode.Minor), TransposeOption.BemolMode),
        };
        
    }
}
