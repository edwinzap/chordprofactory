﻿using ChordProFactory.ChordProComponents;
using System.Collections.Generic;

namespace ChordProFactory.ChordProComponent
{
    public class TabBlock : Block
    {
        public TabBlock()
        {
            TabLines = new List<TabLine>();
        }

        public List<TabLine> TabLines { get; set; }

        public override string PrintToChordProFormat()
        {
            string text = "{start_of_tab}";
            foreach (var line in TabLines)
            {
                text = string.Join(System.Environment.NewLine, text, line.PrintToChordProFormat());
            }
            text = string.Join(System.Environment.NewLine, text, "{end_of_tab}");
            return text;
        }

        public override string PrintToTextFormat()
        {
            string text = string.Empty;
            foreach (var line in TabLines)
            {
                if (string.IsNullOrEmpty(text))
                {
                    text = line.PrintToTextFormat();
                }
                else
                {
                    text = string.Join(System.Environment.NewLine, text, line.PrintToChordProFormat());
                }
            }
            return text;
        }
    }

}
