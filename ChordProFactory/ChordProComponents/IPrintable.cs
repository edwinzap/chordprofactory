﻿namespace ChordProFactory.ChordProComponent
{
    public interface IPrintable
    {
        string PrintToChordProFormat();
        string PrintToTextFormat();
    }
}