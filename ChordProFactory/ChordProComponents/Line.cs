﻿using System;
using System.Collections.Generic;

namespace ChordProFactory.ChordProComponent
{
    public class Line:ChordProElement
    {
        public Line()
        {
            this.Chords = new List<PlacedChord>();
        }

        public List<PlacedChord> Chords { get; set; }

        private string lyric;
        public string Lyric
        {
            get { return lyric; }
            set { lyric = value.TrimEnd(); }
        }

        public override string PrintToTextFormat()
        {
            string chordLine = string.Empty;
            string lyricLine = Lyric;
            int addedSpaces = 0;

            for (int i = 0; i < Chords.Count; i++)
            {
                var chord = Chords[i];

                int spaceDiff = chord.StartIndex - chordLine.Length;
                int spaces = Math.Abs(spaceDiff);
                if (spaces == 0 && i > 0)
                    spaces = 1;

                if (string.IsNullOrEmpty(Lyric) && chord.StartIndex > 0)
                    spaces = 2;

                chordLine +=  new string(' ', spaces) + chord.Chord.ToString();

                if (i > 0)
                {
                    int lastChordStartIndex= chord.StartIndex + addedSpaces;
                    if (spaceDiff < 1 && !string.IsNullOrEmpty(Lyric) && lastChordStartIndex != 0 && Lyric.Length >= lastChordStartIndex) //Chords are too close to each other
                    {
                        lyricLine = lyricLine.Insert(lastChordStartIndex, new string(' ', spaces));
                        addedSpaces += spaces;
                    }
                }
            }
            if (string.IsNullOrEmpty(Lyric))
            {
                return chordLine;
            }
            return string.Join(System.Environment.NewLine, chordLine, lyricLine);
        }
               
        public override string PrintToChordProFormat()
        {
            string text = Lyric;
            int addedSpace = 0;
            foreach (var placedChord in Chords)
            {
                string chordText = placedChord.Chord.PrintToChordProFormat();
                int index = placedChord.StartIndex + addedSpace;
                int indexDiff = text.Length - index;
                if (indexDiff < 0)
                {
                    text += new string(' ', Math.Abs(indexDiff));
                }
                text = text.Insert(index, chordText);
                addedSpace += 2 + placedChord.Chord.ToString().Length;
            }
            return text;
        }

        public override void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            foreach (var chord in Chords)
            {
                chord.Chord.Transpose(steps, option);
            }
        }
    }
}
