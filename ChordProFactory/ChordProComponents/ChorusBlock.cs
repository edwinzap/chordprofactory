﻿using System.Collections.Generic;

namespace ChordProFactory.ChordProComponent
{
    public class ChorusBlock : VerseBlock
    {
        public override string PrintToTextFormat()
        {
            return string.Join(System.Environment.NewLine, "Chorus", "------", base.PrintToTextFormat());
        }

        public override string PrintToChordProFormat()
        {
            string text = "{start_of_chorus}"; //add start_of_chorus tag
            text = string.Join(System.Environment.NewLine, text, base.PrintToChordProFormat()); //Print verse version
            text = string.Join(System.Environment.NewLine, text, "{end_of_chorus}"); //add end_of_chorus tag
            return text;
        }
    }

}
