﻿using System;
using ChordProFactory.MusicalNotation;

namespace ChordProFactory.ChordProComponent
{
    public abstract class Block:ChordProElement
    {
        public override abstract string PrintToTextFormat();
        public override abstract string PrintToChordProFormat();

        public virtual void Transpose(Note targetKey)
        {
            return;
        }
    }

}
