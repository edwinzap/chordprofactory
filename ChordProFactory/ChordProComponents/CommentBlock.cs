﻿namespace ChordProFactory.ChordProComponent
{
    public class CommentBlock: Block
    {
        public CommentBlock() { }

        public CommentBlock(string text)
        {
            Text = text;
        }

        public string Text { get; set; }

        public override string PrintToChordProFormat()
        {
            return string.Concat("{comment: ", Text, "}");
        }

        public override string PrintToTextFormat()
        {
            return Text;
        }
    }

}
