﻿using ChordProFactory.MusicalNotation;
using System.Collections.Generic;

namespace ChordProFactory.ChordProComponent
{
    public class VerseBlock : Block
    {
        public VerseBlock()
        {
            Lines = new List<Line>();
        }
        public List<Line> Lines { get; set ; }

        public override string PrintToChordProFormat()
        {
            if (Lines.Count == 0)
                return string.Empty;

            string text = Lines[0].PrintToChordProFormat();
            for (int i = 1; i < Lines.Count; i++)
            {
                var line = Lines[i];
                text = string.Join(System.Environment.NewLine, text, line.PrintToChordProFormat());
            }
            return text;
        }

        public override string PrintToTextFormat()
        {
            if (Lines.Count == 0)
                return string.Empty;

            string text = Lines[0].PrintToTextFormat();
            for (int i = 1; i < Lines.Count; i++)
            {
                var line = Lines[i];
                text = string.Join(System.Environment.NewLine, text, line.PrintToTextFormat());
            } 
            return text;
        }

        public override void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            foreach (var line in Lines)
            {
                line.Transpose(steps, option);
            }
        }
    }
}
