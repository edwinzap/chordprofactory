﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChordProFactory.ChordProComponent
{
    public class PlacedChord
    {
        public PlacedChord()
        {

        }

        public PlacedChord(Chord chord, int startIndex)
        {
            Chord = chord;
            StartIndex = startIndex;
        }

        public Chord Chord { get; set; }

        public int StartIndex { get; set; }

    }
}
