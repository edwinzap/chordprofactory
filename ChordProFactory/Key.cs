﻿using static ChordProFactory.MusicalNotations;

namespace ChordProFactory
{
    public class Key
    {
        public Note Note { get; set; }
        public Accidental Accidental { get; set; }
        public Mode Mode { get; set; }

        public Key(Note note)
        {
            Note = note;
            Accidental = Accidental.None;
        }

        public Key(Note note, Accidental accidental, Mode mode)
        {
            Note = note;
            Accidental = Accidental;
            Mode = mode;
        }

        public static Key Parse(string input)
        {
            TryParse(input, out Key key);
            return key;
        }

        public static bool TryParse(string input, out Key key)
        {
            key = null;
            //Basic validation check
            if(string.IsNullOrEmpty(input) || input.Length > 3 || input.Length <= 0)
            {
                return false;
            }
            else
            {
                Letter note = Letter.Parse(input[0]);
                key = new Key(note);
                if (input.Length == 2)
                {
                    Accidental accidental = Accidental.Parse(input[1]);
                    key.Accidental = accidental;
                }
                return true;
            }
        }

        override public string ToString()
        {
            string mode = "";
            if(this.Mode == Mode.Minor)
            {
                mode = "m";
            }
            return string.Concat(this.Note.Letter, this.Accidental.Value, mode);
        }

    }
}
