﻿using ChordProFactory.MusicalNotation;
using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace ChordProFactory
{
    public class Chord:ChordProElement
    {
        private const string CHORD_PATTERN = @"^(?<key>(?<note>[A-G])(?<accidental>#|b)?(m(?!aj))?)(?<coloration>.*?)(?:\/?(?<bass>[A-G](?:#|b)?))?$";
        //TODO: validate dim
        public Chord(){}

        public Chord(string chordText)
        {
            var chord = Parse(chordText);
            Key = chord.Key;
            Coloration = chord.Coloration;
            Bass = chord.Bass;
        }

        public Key Key { get; set; }

        public Note Bass { get; set; }

        public string Coloration { get; set; }

        public static Chord Parse(string chordText)
        {
            var regex = new Regex(CHORD_PATTERN);
            var groups = regex.Match(chordText).Groups;

            if(groups.Count == 0)
            {
                throw new FormatException("Invalid chord");
            }

            Chord chord = new Chord();

            chord.Key = Key.Parse(groups["key"].Value);
            chord.Coloration = groups["coloration"].Success? groups["coloration"].Value: string.Empty;
            chord.Bass = groups["bass"].Success? Note.Parse(groups["bass"].Value): null;

            return chord;
        }

        public static bool TryParse(string chordText, out Chord chord)
        {
            chord = null;
            try
            {
                chord = Parse(chordText);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public override string PrintToChordProFormat()
        {
            return string.Concat("[", this.ToString(), "]");
        }

        public override string PrintToTextFormat()
        {
            return this.ToString();
        }

        public override string ToString()
        {
            if(Key == null)
            {
                return "";
            }
            string chordString = Key.ToString() + Coloration;
            if(Bass != null)
            {
                chordString = chordString + "/" + Bass.ToString();
            }
            return chordString;
        }

        public override void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            this.Key.Note.Transpose(steps, option);
            this.Bass?.Transpose(steps, option);
        }
    }

}
