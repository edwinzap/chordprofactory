﻿using ChordProFactory.MusicalNotation;

namespace ChordProFactory
{
    public interface ITransposable
    {
        void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode);
    }

    public enum TransposeOption
    {
        SharpMode,
        BemolMode
    }
}