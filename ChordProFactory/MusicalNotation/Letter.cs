﻿using ChordProFactory;
using System.Collections.Generic;
using System.Linq;

namespace ChordProFactory.MusicalNotation
{
    public class Letter : CustomEnum<char>
    {
        private Letter(char value) : base(value)
        {
        }

        private static readonly List<Letter> letters = new List<Letter>()
        {
            A, B, C, D, E, F, G
        };

        public static readonly Letter A = new Letter('A');
        public static readonly Letter B = new Letter('B');
        public static readonly Letter C = new Letter('C');
        public static readonly Letter D = new Letter('D');
        public static readonly Letter E = new Letter('E');
        public static readonly Letter F = new Letter('F');
        public static readonly Letter G = new Letter('G');


        public static bool TryParse(char input, out Letter letter)
        {
            letter = null;
            string value = input.ToString().ToUpper();
            switch (input){
                case 'A':
                    letter = A;
                    break;
                case 'B':
                    letter = B;
                    break;
                case 'C':
                    letter = C;
                    break;
                case 'D':
                    letter = D;
                    break;
                case 'E':
                    letter = E;
                    break;
                case 'F':
                    letter = F;
                    break;
                case 'G':
                    letter = G;
                    break;
            }

            return letter != null;
        }

        public static Letter Parse(char input)
        {
            TryParse(input, out Letter letter);
            return letter;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Letter letter = (Letter)obj;
            return letter.Value == this.Value;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}