﻿using System.Collections.Generic;
using System.Linq;

namespace ChordProFactory.MusicalNotation
{
    public class Note:ITransposable
    {
        private static readonly List<Note> notesSharp = new List<Note>
        {
            new Note(Letter.A, Accidental.None),
            new Note(Letter.A, Accidental.Sharp),
            new Note(Letter.B, Accidental.None),
            new Note(Letter.C, Accidental.None),
            new Note(Letter.C, Accidental.Sharp),
            new Note(Letter.D, Accidental.None),
            new Note(Letter.D, Accidental.Sharp),
            new Note(Letter.E, Accidental.None),
            new Note(Letter.F, Accidental.None),
            new Note(Letter.F, Accidental.Sharp),
            new Note(Letter.G, Accidental.None),
            new Note(Letter.G, Accidental.Sharp),
        };

        private static readonly List<Note> notesBemol = new List<Note>
        {
            new Note(Letter.A, Accidental.None),
            new Note(Letter.B, Accidental.Bemol),
            new Note(Letter.B, Accidental.None),
            new Note(Letter.C, Accidental.None),
            new Note(Letter.D, Accidental.Bemol),
            new Note(Letter.D, Accidental.None),
            new Note(Letter.E, Accidental.Bemol),
            new Note(Letter.E, Accidental.None),
            new Note(Letter.F, Accidental.None),
            new Note(Letter.G, Accidental.Bemol),
            new Note(Letter.G, Accidental.None),
            new Note(Letter.A, Accidental.Bemol),
        };

        public Note(Letter letter) : this(letter, Accidental.None)
        {
            
        }

        public Note(Letter letter, Accidental accidental)
        {
            Letter = letter;
            Accidental = accidental;
        }

        public Letter Letter { get; private set; }
        public Accidental Accidental { get; private set; }

        public override string ToString()
        {
            return string.Concat(Letter.ToString(), Accidental.ToString());
        }
        
        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Note note = (Note)obj;
            return note.Accidental.Equals(this.Accidental) && note.Letter.Equals(this.Letter);
        }

        public static bool TryParse(string input, out Note note)
        {
            note = null;
            if (input.Length > 2)
            {
                return false;
            }

            bool resultLetter = Letter.TryParse(input[0], out Letter letter);
            if (!resultLetter)
                return false;

            Accidental accidental = Accidental.None;
            if (input.Length == 2)
            {
                bool resultAccidental = Accidental.TryParse(input[1], out accidental);
                if (!resultAccidental)
                {
                    return false;
                }
            }

            note = new Note(letter, accidental);
            return true;
        }

        public static Note Parse(string input)
        {
            TryParse(input, out Note note);
            return note;
        }

        public void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            if (steps < 0)
                steps = 12 + steps;

            if(steps%12 == 0)
                return;

            if(this.Equals(new Note(Letter.C, Accidental.Bemol)))
            {
                this.Letter = Letter.B;
                this.Accidental = Accidental.None;
            }
            else if (this.Equals(new Note(Letter.F, Accidental.Bemol)))
            {
                this.Letter = Letter.E;
                this.Accidental = Accidental.None;
            }
            else if (this.Equals(new Note(Letter.E, Accidental.Sharp)))
            {
                this.Letter = Letter.F;
                this.Accidental = Accidental.None;
            }
            else if (this.Equals(new Note(Letter.B, Accidental.Sharp)))
            {
                this.Letter = Letter.C;
                this.Accidental = Accidental.None;
            }

            List<Note> notes;
            if(this.Accidental == Accidental.Sharp)
            {
                notes = notesSharp;
            }
            else
            {
                notes = notesBemol;
            }

            List<Note> notesTargetMode;
            if (option == TransposeOption.SharpMode)
            {
                notesTargetMode = notesSharp;
            }
            else
            {
                notesTargetMode = notesBemol;
            }

            int index = notes.IndexOf(notes.Where(x => x.Equals(this)).FirstOrDefault());
            int transposedIndex = (index + steps) % 12;


            //return notesTargetMode[tr  ansposedIndex]; //TODO: return void et remplacer le this par une nouvelle note
            var transposedNote = notesTargetMode[transposedIndex];
            this.Accidental = transposedNote.Accidental;
            this.Letter = transposedNote.Letter;
        }

        public static int GetStepsBetweenNotes(Note firstNote, Note secondNote)
        {
            List<Note> firstNotes = notesSharp;
            List<Note> secondNotes = notesSharp;

            if (firstNote.Accidental.Value == Accidental.Bemol.Value)
                firstNotes = notesBemol;

            if (secondNote.Accidental.Value == Accidental.Bemol.Value)
                secondNotes = notesBemol;


            int firstNoteIndex = firstNotes.IndexOf(firstNotes.Where(n => n.Equals(firstNote)).FirstOrDefault());
            int secondNoteIndex = secondNotes.IndexOf(secondNotes.Where(n => n.Equals(secondNote)).FirstOrDefault());

            return secondNoteIndex - firstNoteIndex;
        }
        public override int GetHashCode()
        {
            var hashCode = -1822406274;
            hashCode = hashCode * -1521134295 + EqualityComparer<Letter>.Default.GetHashCode(Letter);
            hashCode = hashCode * -1521134295 + EqualityComparer<Accidental>.Default.GetHashCode(Accidental);
            return hashCode;
        }
    }
}
