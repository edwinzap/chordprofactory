﻿using ChordProFactory;

namespace ChordProFactory.MusicalNotation
{
    public class Accidental : CustomEnum<char>
    {
        public Accidental(char value) : base(value)
        {
        }

        public static Accidental None { get { return new Accidental(char.MinValue); } }
        public static Accidental Bemol { get { return new Accidental('b'); } }
        public static Accidental Sharp { get { return new Accidental('#'); } }


        public static bool TryParse(char input, out Accidental accidental)
        {
            if (input == 'b')
            {
                accidental = Bemol;
                return true;
            }

            if (input == '#')
            {
                accidental = Sharp;
                return true;
            }

            accidental = None;
            return false;
        }

        public static Accidental Parse(char input)
        {
            TryParse(input, out Accidental accidental);
            return accidental;
        }

        public override string ToString()
        {
            return Value == char.MinValue? "": Value.ToString();
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Accidental accidental = (Accidental)obj;
            return accidental.Value == this.Value;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
