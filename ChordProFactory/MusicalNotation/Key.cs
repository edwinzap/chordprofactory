﻿namespace ChordProFactory.MusicalNotation
{
    public class Key
    {
        public Note Note { get; set; }
        public Mode Mode { get; set; }

        public Key(Note note)
        {
            Note = note;
            Mode = Mode.Major;
        }

        public Key(Note note, Mode mode)
        {
            Note = note;
            Mode = mode;
        }

        public static Key Parse(string input)
        {
            TryParse(input, out Key key);
            return key;
        }

        public static bool TryParse(string input, out Key key)
        {
            key = null;
            //Basic validation check
            if(string.IsNullOrEmpty(input) || input.Length > 3 || input.Length <= 0)
            {
                return false;
            }

            Mode mode = Mode.Major;
            if (input.Length > 1)
            {
                if (input[1] == 'm')
                {
                    mode = Mode.Minor;
                    input = input.Remove(1); //Remove the mode value from the input
                }
            }

            bool result = Note.TryParse(input, out Note note);
            if (!result)
                return false;

            key = new Key(note, mode);
            return true;
        }

        override public string ToString()
        {
            string modeString = "";
            if(this.Mode == Mode.Minor)
            {
                modeString = "m";
            }
            return string.Concat(this.Note.ToString(),  modeString);
        }
    }
}
