﻿using ChordProFactory.ChordProComponent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ChordProFactory
{
    public abstract class BaseParser
    {
        protected const string KEY_PATTERN = @"([A-G](#|b)?m?)";
        protected const string CHORD_PATTERN = @"(?<chord>(?<key>(?<note>[A-G])(?<accidental>#|b)?)(m(?!(aj|in)))?(?<coloration>(dim|aug|maj|min|sus|\d)*)?(?:\/?(?<bass>[A-G](?:#|b)?))?)";
        protected const string FINGER_PATTERN = @"[0-9]{1,2}";

        protected const string TAB_LINE_PATTERN = @"^[aA-zZ]\|[ \-\d\|]*$";


        public abstract ChordPro ParseSong(string file);

        protected bool IsBlankLine(string line)
        {
            return string.IsNullOrEmpty(line.Trim());
        }

        protected bool IsTabLine(string line)
        {
            return Regex.IsMatch(line.Trim(), TAB_LINE_PATTERN);
        }
    }
}
