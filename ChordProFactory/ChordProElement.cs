﻿using ChordProFactory.ChordProComponent;

namespace ChordProFactory
{
    public abstract class ChordProElement : ITransposable, IPrintable
    {
        public abstract string PrintToChordProFormat();
        public abstract string PrintToTextFormat();
        public virtual void Transpose(int steps, TransposeOption option = TransposeOption.SharpMode)
        {
            return;
        }
    }
}
