﻿using ChordProFactory.ChordProComponent;
using ChordProFactory.ChordProComponents;
using ChordProFactory.MusicalNotation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ChordProFactory
{
    public class ChordProFormatParser:BaseParser
    {
        public override ChordPro ParseSong(string file)
        {
            String[] lines = file.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var chordPro = new ChordPro();

            for (int i = 0; i < lines.Length - 1; i++)
            {
                var line = lines[i];


                if (string.IsNullOrEmpty(chordPro.Title) && TryParseTitle(line, out string title))
                {
                    chordPro.Title = title;
                    continue;
                }

                if (string.IsNullOrEmpty(chordPro.Subtitle) && TryParseSubtitle(line, out string subtitle))
                {
                    chordPro.Subtitle = subtitle;
                    continue;
                }

                if (string.IsNullOrEmpty(chordPro.Artist) && TryParseArtist(line, out string artist))
                {
                    chordPro.Artist = artist;
                    continue;
                }

                if (string.IsNullOrEmpty(chordPro.Album) && TryParseAlbum(line, out string album))
                {
                    chordPro.Album = album;
                    continue;
                }

                if (chordPro.Key == null && TryParseKey(line, out Key key))
                {
                    chordPro.Key = key; //TODO: Key peut changer durant le morceau !
                    continue;
                }

                if (chordPro.Duration.TotalSeconds == 0 && TryParseDuration(line, out TimeSpan duration))
                {
                    chordPro.Duration = duration;
                    continue;
                }
                //Parse content

                //Check comment
                if (TryParseComment(line, out CommentBlock comment))
                {
                    chordPro.Blocks.Add(comment);
                    continue;
                }

                //Check chorus
                if (line.Equals("{soc}") || line.Equals("{start_of_chorus}"))
                {
                    var chorus = new ChorusBlock();
                    line = lines[++i];
                    while (!(line.Equals("{eoc}") || line.Equals("{end_of_chorus}")))
                    {
                        if (TryParseChords(line, out Line chorusLine))
                        {
                            chorus.Lines.Add(chorusLine);
                        }

                        if (i < lines.Length - 1)
                            line = lines[++i];
                        else
                            break;
                    }
                    chordPro.Blocks.Add(chorus);
                    continue;
                }

                //Check tabs
                if ((line.Equals("{sot}") || line.Equals("{start_of_tab}")))
                {
                    line = lines[++i];
                    string tab = line;
                    while (!(lines[i + 1].Equals("{eot}") || lines[i + 1].Equals("{end_of_tab}")))
                    {
                        if (i < lines.Length - 1)
                        {
                            line = lines[++i];
                            if (!string.IsNullOrEmpty(line))
                            {
                                tab = string.Join(System.Environment.NewLine, tab, line);
                            }
                        }
                        else
                            break;
                    }

                    if (TryParseTabs(tab, out TabBlock tabBlock))
                    {
                        chordPro.Blocks.Add(tabBlock);
                    }
                    i++;
                    continue;
                }

                //Other lines
                if (string.IsNullOrEmpty(line.Trim()))
                {
                    chordPro.Blocks.Add(new BlankBlock());
                }
                else
                {
                    var verse = new VerseBlock();
                    while (!string.IsNullOrEmpty(line.Trim()))
                    {
                        if (TryParseChords(line, out Line verseLine))
                        {
                            verse.Lines.Add(verseLine);
                        }

                        if (i < lines.Length - 1)
                            line = lines[++i];
                        else
                            break;
                    }
                    chordPro.Blocks.Add(verse);
                    chordPro.Blocks.Add(new BlankBlock());
                }
            }
            return chordPro;
        }

        /// <summary>
        /// Converts the value into an artist. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="album">Artist</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseArtist(string text, out string artist)
        {
            string paramPattern = @"[Aa](rtist)?";
            string valuePattern = @".*?";

            return TryMatchParam(text, paramPattern, valuePattern, out artist);
        }

        /// <summary>
        /// Parse the value into a title. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="album">Title</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseTitle(string s, out string title)
        {
            string paramPattern = @"[Tt](itle)?";
            string valuePattern = @".*?";

            return TryMatchParam(s, paramPattern, valuePattern, out title);

        }

        /// <summary>
        /// Parse the value into a subtitle. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="subtitle">Subtitle</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseSubtitle(string s, out string subtitle)
        {
            string paramPattern = @"([Ss]ubtitle|[Ss]t)";
            string valuePattern = @".*?";

            return TryMatchParam(s, paramPattern, valuePattern, out subtitle);
        }

        /// <summary>
        /// Parse the value into a key. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="key">Key</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseKey(string s, out Key key)
        {
            key = null;
            string paramPattern = @"[Kk](ey)?";
            string valuePattern = KEY_PATTERN;

            bool result = TryMatchParam(s, paramPattern, valuePattern, out string keyString);
            if (result)
            {
                return Key.TryParse(keyString, out key);
            }

            return false;

        }

        /// <summary>
        /// Parse the value into an album. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="album">Album</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseAlbum(string s, out string album)
        {
            string paramPattern = @"[Aa]lbum";
            string valuePattern = @".*?";

            return TryMatchParam(s, paramPattern, valuePattern, out album);
        }

        /// <summary>
        /// Parse the value into a capo. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="capo">Capo</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseCapo(string s, out int capo)
        {
            capo = 0;
            string paramPattern = @"[Cc]apo";
            string valuePattern = @"(1[0-1]|[1-9])";

            bool result = TryMatchParam(s, paramPattern, valuePattern, out string capoString);
            if (result)
            {
                int.TryParse(capoString, out capo);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Parse the value into a duration. A return value specifies if the parsing is successful.
        /// </summary>
        /// <param name="s">Inpput value</param>
        /// <param name="duration">Duration</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseDuration(string text, out TimeSpan duration)
        {
            string paramPattern = @"[Dd]uration";
            string valuePattern = @"[0-5]?[0-9]:[0-5][0-9]";

            bool result = TryMatchParam(text, paramPattern, valuePattern, out string durationString);

            if (result)
            {
                return TimeSpan.TryParse("0:" + durationString, out duration); ;
            }
            duration = new TimeSpan();
            return false;
        }

        /// <summary>
        /// Parse the value into a line with text and chords
        /// </summary>
        /// <param name="text">Input value</param>
        /// <param name="line">Output Line</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseChords(string text, out Line line)
        {
            line = new Line();
            string pattern = @"\[" + CHORD_PATTERN + @"\]";
            var matches = Regex.Matches(text, pattern);
            int chordsLength = 0;
            foreach (Match match in matches)
            {
                var chordString = match.Groups["chord"].Value;
                int chordIndex = match.Index - chordsLength;
                chordsLength += match.Length;

                //Chords
                var chord = new PlacedChord(new Chord(chordString), chordIndex);
                line.Chords.Add(chord);
            }

            //Lyric
            string lyric = Regex.Replace(text, pattern, "");
            line.Lyric = lyric.TrimEnd();

            return true;
        }

        /// <summary>
        /// Parse the value into a TabBlock
        /// </summary>
        /// <param name="text">Input text</param>
        /// <param name="tab">Output tab</param>
        /// <returns>A return value indicates whether the conversion succeeded</returns>
        private bool TryParseTabs(string text, out TabBlock tab)
        {
            tab = new TabBlock();
            var lines = text.Split(System.Environment.NewLine);
            foreach (var line in lines)
            {
                if (Regex.IsMatch(line, @"^[aA-zZ]\|"))
                {
                    var tabLine = new TabLine();
                    tabLine.Text = Regex.Replace(line, FINGER_PATTERN, "");
                    var matches = Regex.Matches(line, FINGER_PATTERN);
                    foreach (Match match in matches)
                    {
                        tabLine.Fingers.Add(match.Index, int.Parse(match.Value));
                    }
                    tab.TabLines.Add(tabLine);
                }
            }
            return true;
        }

        /// <summary>
        /// Converts the value into a CommentBlock
        /// </summary>
        /// <param name="text">Input text</param>
        /// <param name="comment">Output CommentBlock</param>
        /// <returns></returns>
        private bool TryParseComment(string text, out CommentBlock comment)
        {
            comment = new CommentBlock();

            string paramPattern = @"[Cc](omment)?";
            string valuePattern = @".*?";
            if (TryMatchParam(text, paramPattern, valuePattern, out string commentString))
            {
                comment.Text = commentString;
                return true;
            }
            comment = null;
            return false;
        }

        private bool TryMatchParam(string text, string paramPattern, string valuePattern, out string param)
        {
            string pattern = BuildPattern(paramPattern, valuePattern);
            var matches = Regex.Match(text, pattern);

            if (matches.Groups.Count > 0 && !string.IsNullOrEmpty(matches.Groups["value"].Value))
            {
                param = matches.Groups["value"].Value.Trim();
                return true;
            }
            param = null;
            return false;
        }

        private string BuildPattern(string paramPattern, string valuePattern)
        {
            string whiteSpacePattern = @"\s*";
            return string.Concat("{", whiteSpacePattern, paramPattern, ":", whiteSpacePattern, "(?<value>", valuePattern, ")", whiteSpacePattern, "}");
        }
    }
}
